# IP Video Archiver and Analytics
This project receives IP Multicast RTP/H.264 video streams from Axis Communication IP Video Surveillance cameras and writes H.264 files. It effectively is a lightweight Python video management software solution. 

The genesis of this project stemmed from a need to validate the performance characteristics and limits of storage systems used for video archive. 

A typical video surveillance deployment maintains a one to one (1:1) relationship between a camera and the receiver function of the video management software. This solution uses the IP network to allow for a one to many (1:n) relationship between a camera and the receiver by using multicast routing to replicate the original video feed to many receivers.

Installing and maintaining surveillance cameras in a test lab environment for the purpose of performance testing is time consuming and unwieldy 

By using the multicast capability of the Axis cameras, the network can replicate a single video feed into any number of network interfaces. By configuring trunk port(s) between a Linix host and the L3 switch, any number of VLANs (virtual LANs) can be configured on a single physical interface. By invoking the Python code in this solution to listen for multicast packets on each VLAN, or multiple receivers on the same VLAN and UDP port, a single camera feed can be received from the network and written to the storage system from multiple receivers.

A challenge in developing validated solutions at scale is how to accurately generate data to report performance of the network, compute and storage. There is camera simulator software, like the Axis network camera emulator. This simulator runs on Windows 2008 R2 on physical machines. The simulator is configured to connect to a physical camera, accept and record a video feed, and replicate that video feed emulating some number of virtual cameras. Replaying a short video loop doesn't offer the variability of an actual video surveillance deployment, because the scene captured is a subset in time of the actual scene.

While virtual camera simulators and other performance tools can be used to benchmark storage performance, video from actual cameras provides a more realistic representation of a video deployment. 

## Topology

![Topology](documentation/Topology_Overview.png "Solution Topology")

## Background
Details on my prior experience in video surveillance can be found in [BACKGROUND](./documentation/BACKGROUND.md). The Networkers session [BRKEVT-2311 - Network Design and Implementation for IP Video Surveillance](https://www.slideshare.net/joelwking/brkevt2311joekingpbrpptx) discusses the fundamentals of deploying IP video surveillance. It provides a foundation on selecting the type of IP cameras, the placement, field of view, resolution and frame rate to address the safety and security requirements of the organization.

## Analytics
The real value in video is the advanced analytics of the video streams which provide business value over and above the loss control and forensic analysis of nefarious activity.  Cisco Meraki emphasis the use of out-of-the box analytics for people and vehicle detection along with a partner ecosystem to use the camera as a platform for retail analytics. Axis provides a platform for 3rd party software applications developed which run onboard Axis cameras. AXIS Object Analytics is available at no additional cost on compatible Axis network cameras, providing AI-based algorithms.

One enhancement to this solution is the feature of uploading the MP4 files to AWS S3 buckets and then use [Amazon Rekognition](https://docs.aws.amazon.com/rekognition/latest/dg/what-is.html) for video analysis of the videos.

## Multicast Primer
IP multicast is a means to conserve bandwidth and deliver packets to multiple receivers without adding any additional burden on the application sending the packets. Multimedia streaming applications, which include IP Video Surveillance, can deliver their content via multicast. This section shows how to configure the Axis camera software to generate RTP/H.264 packets over multicast and a brief overview of how multicast works.

While the sender and receiver of the IPmc stream do not incur any additional burden of sending or receiving a IPmc stream, regardless of the number of receivers present in the network, the routers in the network do consume additional CPU resources.  The routers consume additional resources at the control plane of IP multicast and the data replication function. 

The control plane is made up of Internet Group Management Protocol (IGMP) and Protocol Independent Multicast (PIM). Routers listen to IGMP messages from hosts on their local networks and periodically send out queries to discover which groups are active or inactive.

The Python [socket](https://docs.python.org/3/library/socket.html) module will send out IGMP `Join Group` when started and `Leave Group` when terminated. There is no provision to send or received periodic IGMP messages.

### Customizing IGMP (Optional)
Cisco routers can be [configured](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/ipmulti_igmp/configuration/xe-16/imc-igmp-xe-16-book/imc-customizing-igmp.html
) to forward multicast traffic regardless of any connected IGMP hosts. 

Configure static group membership entries on an interface attached to the receiving host. This method allows fast switching on the IOS router.

```pre
interface gigabitethernet 1
  ip igmp static-group 239.201.174.23
```

### Configure the Axis Camera
The cameras can be configured manually using the web interface and selecting `Setup`. There are also Python libraries and API calls which can be used for larger deployments.

#### Multicast Configuration
Using the Axis web interface and setup option, the RTP port range and multicast settings are configured under System Options > Network > RTP. By selecting the `Always Multicast Video` multicast streaming is enabled without opening an RTSP session to the camera.  The multicast group (IP) address, port number, and Time-To-Live value are configured as well.

Suggested configuration values for the Axis camera.

| RTP Setting | Recommended Value | Description |
|:--------------|:-----------:|:------------|
| Start port    | 50000 | Must match the value passed to the receiving program | 
| End port      | 50099 | Select a value higher than the start port |
| Video Address | 239.238.108.64 (*1) | Must match the value passed to the receiving program |
| Video Port    | 0 | Use the default value |
| Time to Live  | 5 | Limits the number of router hops for these packets |
| Always Multicast Video | ✓ | Select the profile `H.264`  or other appropriate protocol (*2)|

> **Note:** (1) Within a network of cameras and receivers, each camera should be assigned a unique multicast group address. (2) The profiles listed are `balanced`, `quality`, `bandwidth` and `mobile` in addition to `H.264`. There is a current **TODO** to evaluate the characteristics of these profiles. The profiles appear to ignore the GOV setting unless `H.264` is selected. The `quality` sends few, if any P-Frames. The `balanced` sends fragmented P-Frames, the `bandwidth` and `mobile` send few I-frames.

IPV4 addresses (groups) in the range 224.0.0.0 and 239.255.255.255 are reserved for multicasting. 

For example, assume we have these cameras in our test environment.

| Camera | Location | MAC address | Management IP | Multicast Group |
|:--------------|:-----------:|:------------|:------------|---------|
| AXIS M1004-W | Entrance | AC:CC:8E:20:24:C8 | [192.168.5.90](http://192.168.5.90) | 239.238.5.90
| AXIS M1014   | Shop     | 00:40:8C:F9:AE:17 | [192.168.5.91](http://192.168.5.91) | 239.238.5.91
| AXIS M3004   | Office   | 00:40:8C:E2:6C:40 | [192.168.5.92](http://192.168.5.92) | 239.238.5.92

Associating the last two octets of the unicast IP address and the multicast group is a recommended practice.

#### Video Stream Settings
Select Basic Setup > Video Stream -> H.264. The default GOV (Group of Video) value is 32. Increasing the number conserves bandwidth by sending more P-Frames between I-Frames (key frames). Selecting the default of `Variable bit rate` (VBR) provides a pre-defined level of image quality. With `Constant Bit Rate`, the image quality varies but the bit rate is more constant.

The placement of the camera, and the amount of motion within the view of the camera are a factor in the data rate for VBR. This is one advantage of using real cameras in performance testing rather than a simulator which loops over a recorded image. The different lighting conditions due to sunlight, clouds, interior lighting impact the complexity of the image. Additionally, people or traffic moving also influences the complexity of the video.

#### RTCP packets
RTCP (Real-Time Transmission Control Protocol) is the control plane for RTP (Real-time Transport Protocol). RTCP is used to provide control and statistical information about an RTP media streaming session.

The Axis camera will send out a RTCP packet prior to starting the RTP stream using a source and destination port of 50001 (one higher than the port number for the RTP stream that contains the Sender SSRC value. This value is the Synchronization Source identifier in each of the RTP packets that follow. However, because we may begin receiving packets subsequent to the dialog box being selected, these RTCP packets are not available to the receiving program.

The receiving program will not receive the RTCP packets, as the program is listening for UDP packets on the RTP port configured on the camera, not the next higher port.

## Installation
In the root of the repository there is a `Vagrantfile` which can be used for testing and evaluation. The example configuration assumes an Axis camera is attached to the Ethernet ("Realtek PCIe GBE Family Controller") interface of a Windows 10 laptop with Vagrant and Virtualbox installed.  This interface is bridged between the virtual machine and the Windows 10 host. The default IP address (factory default) of the Axis camera is 192.168.0.90, the Windows 10 laptop is assigned 192.168.0.1 and the virtual machine is assigned 192.168.0.2.

![Topology](documentation/Test_Topology.png "Test_Topology")

Make a copy of the `Vagrantfile` on the host machine (Windows 10 laptop) in a directory and issue the command:

```shell
vagrant up
```
After the virtual machine is booted, log on using:

```shell
vagrant ssh
```
In the home directory, clone the repository and enter the directory.

```shell
git clone https://gitlab.com/joelwking/ip_video_aa.git
cd ip_video_aa
```

Install the required packages:

```shell
 python3 -m pip install -r requirements.txt
````
## Program Schematic

![Topology](documentation/program_schematic.png "Program Schematic")


## Usage
Determine the camera's configured multicast group address, port number, the camera name or some unique identifier for the log and the IP address of the Linux interface connected to the camera network.

## Create Session Description Protocol files

Refer to [WIRESHARK](./documentation/WIRESHARK.md) under `Parameter Sets` on how to create the SDP files. Enhancements to this solution will automate creating these metadata files.

### Environment variables
All program input is specified using Linux environment variables. Refer to the following table. The default values are shown.

| Variable Name | Default     | Description |
|:--------------|:-----------:|:------------|
| AA_DPORT |  50000 | The UDP port configured on the camera for RTP packets
| AA_IF_IP | 192.168.0.1 | The interface IP address to identify the interface to listen on
| AA_MULTICAST_GROUP |  239.201.174.23 | The multicast group address
| AA_BUFFER_SIZE | 16384 | Size of the receive buffer, in bytes, at 4096, you may encounter packet loss                  
| AA_LOGLEVEL | 20 | The logging level DEBUG=10, INFO=20, WARNING=30, ERROR=40 and CRITICAL=50
| AA_LOG_FILE | None | The name of a log file, if specified, log output is written (appended) to this file   
| AA_MAX_IO_BYTES | 4 |  Write output buffer to disk every n MBYTES, default 4MB
| AA_SD_FILENAME | default_sdp | Session Description Protocol (SDP) filename (basename)
| AA_CAMERA | AXIS | Prefix for output files, recommended using the camera name to associate the file with a specific camera 
| AA_DIRECTORY | './' |  Directory to read SDP files and write output files
| AA_MAX_FILE_OPEN | 5 | Number of minutes elapsed time to leave an output file open, before closing and opening a new file, default 5 minutes   

### Determine the IP address of the interface
Verify the IP addresses of the virtual machine, they should look similar to the following:

```shell
vagrant@ubuntu-bionic:~/ip_video_aa$ ifconfig -a
enp0s3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.2.15  netmask 255.255.255.0  broadcast 10.0.2.255
        inet6 fe80::70:dfff:fe2f:2ac8  prefixlen 64  scopeid 0x20<link>
        ether 02:70:df:2f:2a:c8  txqueuelen 1000  (Ethernet)
        RX packets 86302  bytes 127647882 (127.6 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 27230  bytes 1839712 (1.8 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

enp0s8: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.56.200  netmask 255.255.255.0  broadcast 192.168.56.255
        inet6 fe80::a00:27ff:feb6:8abf  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:b6:8a:bf  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 12  bytes 936 (936.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

enp0s9: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.2  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::a00:27ff:fe54:3d40  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:54:3d:40  txqueuelen 1000  (Ethernet)
        RX packets 63  bytes 3780 (3.7 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 12  bytes 936 (936.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```

Configure the environment variables to configure the interface, camera name and multicast group.

```shell
export AA_IF_IP=192.168.0.2
export AA_CAMERA=AXISM1014
export AA_MULTICAST_GROUP=239.201.174.23
export AA_SD_FILENAME=default_sdp
```

Enter the **library** directory (`cd library`) and execute the program. Use CTL + C to exit.

```shell
vagrant@ubuntu-bionic:~/ip_video_aa/library$ python3 capture_video.py
2022-07-24 18:30:25,864 - AXISM1014 - INFO - Listening 239.201.174.23 50000 on interface: 192.168.0.2
2022-07-24 18:30:25,864 - AXISM1014 - INFO - Reading SDP file: ./default_sdp.pps
2022-07-24 18:30:25,864 - AXISM1014 - INFO - Reading SDP file: ./default_sdp.sps
2022-07-24 18:30:25,893 - AXISM1014 - INFO - Opening file: .//AXISM1014_2022_07_24_183025.264
2022-07-24 18:30:25,894 - AXISM1014 - INFO - Bytes: 0, Packets: 1, Files: 1
2022-07-24 18:30:25,894 - AXISM1014 - INFO - I-Frame: Fragmentation unit A (FU-A) 28, located the first I-frame!
2022-07-24 18:32:13,078 - AXISM1014 - INFO - RTP sequence number wrapped: 65535::0
2022-07-24 18:34:29,561 - AXISM1014 - INFO - RTP sequence number wrapped: 65535::0
2022-07-24 18:35:25,923 - AXISM1014 - INFO - Opening file: .//AXISM1014_2022_07_24_183525.264
2022-07-24 18:35:25,925 - AXISM1014 - INFO - Bytes: 192906686, Packets: 143311, Files: 2
2022-07-24 18:35:25,926 - AXISM1014 - INFO - I-Frame: Fragmentation unit A (FU-A) 28, located the first I-frame!
^C2022-07-24 18:35:28,898 - AXISM1014 - INFO - Interrupt SIGNAL:2
```

### Convert the H.264 file to MP4 format
Converting the H.264 output files to MP4 format enables viewing the video from the Axis camera on the VLC media player (or other video player, e.g. Windows Media Player).

The utility [`ffmpeg`](https://ffmpeg.org/) is installed in the development Docker container (see `Dockerfile`). This utility is a prereq for OpenCV. It is also installed in the VirtualBox Linux instance.

>Note: Issue the command with `-loglevel debug` to see detailed log information.

```shell
vagrant@ubuntu-bionic:~/ip_video_aa/library$ ffmpeg -i AXISM1014_2022_07_24_183025.264 AXISM1014_2022_07_24_183025.mp4
ffmpeg version 3.4.11-0ubuntu0.1 Copyright (c) 2000-2022 the FFmpeg developers
  built with gcc 7 (Ubuntu 7.5.0-3ubuntu1~18.04)
  configuration: --prefix=/usr --extra-version=0ubuntu0.1 --toolchain=hardened --libdir=/usr/lib/x86_64-linux-gnu --incdir=/usr/

  [snip]

[h264 @ 0x5628b8e4e800] Stream #0: not enough frames to estimate rate; consider increasing probesize
Input #0, h264, from 'AXISM1014_2022_07_24_183025.264':
  Duration: N/A, bitrate: N/A
    Stream #0:0: Video: h264 (Main), yuv420p(tv, bt709, progressive), 1280x720 [SAR 1:1 DAR 16:9], 25 fps, 25 tbr, 1200k tbn, 50 tbc
Stream mapping:
  Stream #0:0 -> #0:0 (h264 (native) -> h264 (libx264))
Press [q] to stop, [?] for help
[libx264 @ 0x5628b8e51f40] using SAR=1/1
[libx264 @ 0x5628b8e51f40] using cpu capabilities: MMX2 SSE2Fast SSSE3 SSE4.2
[libx264 @ 0x5628b8e51f40] profile High, level 3.1
[libx264 @ 0x5628b8e51f40] 264 - core 152 r2854 e9a5903 - H.264/MPEG-4 AVC codec - Copyleft 2003-2017 - http://www.videolan.org/x264.html - options: cabac=1 ref=3 deblock=1:0:0 analyse=0x3:0x113 me=hex subme=7 psy=1 psy_rd=1.00:0.00 mixed_ref=1 me_range=16 chroma_me=1 trellis=1 8x8dct=1 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=-2 threads=3 lookahead_threads=1 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=2 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=40 rc=crf mbtree=1 crf=23.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 ip_ratio=1.40 aq=1:1.00
Output #0, mp4, to 'AXISM1014_2022_07_24_183025.mp4':
  Metadata:
    encoder         : Lavf57.83.100
    Stream #0:0: Video: h264 (libx264) (avc1 / 0x31637661), yuv420p, 1280x720 [SAR 1:1 DAR 16:9], q=-1--1, 25 fps, 12800 tbn, 25 tbc
    Metadata:
      encoder         : Lavc57.107.100 libx264
    Side data:
      cpb: bitrate max/min/avg: 0/0/0 buffer size: 0 vbv_delay: -1
frame= 4435 fps= 20 q=-1.0 Lsize=   88304kB time=00:02:57.28 bitrate=4080.5kbits/s speed=0.812x
video:88250kB audio:0kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 0.061014%
[libx264 @ 0x5628b8e51f40] frame I:18    Avg QP:19.95  size:107221
[libx264 @ 0x5628b8e51f40] frame P:1117  Avg QP:22.97  size: 49786
[libx264 @ 0x5628b8e51f40] frame B:3300  Avg QP:26.84  size:  9947
[libx264 @ 0x5628b8e51f40] consecutive B-frames:  0.8%  0.0%  0.0% 99.2%
[libx264 @ 0x5628b8e51f40] mb I  I16..4: 12.7% 43.1% 44.1%
[libx264 @ 0x5628b8e51f40] mb P  I16..4:  5.7%  6.4%  1.5%  P16..4: 41.8% 22.6% 14.4%  0.0%  0.0%    skip: 7.7%
[libx264 @ 0x5628b8e51f40] mb B  I16..4:  0.2%  0.1%  0.0%  B16..8: 39.4%  4.8%  1.4%  direct:11.2%  skip:42.8%  L0:57.3% L1:35.1% BI: 7.6%
[libx264 @ 0x5628b8e51f40] 8x8 transform intra:46.0% inter:29.7%
[libx264 @ 0x5628b8e51f40] coded y,uvDC,uvAC intra: 50.9% 68.9% 9.8% inter: 21.7% 27.1% 0.6%
[libx264 @ 0x5628b8e51f40] i16 v,h,dc,p: 37% 20% 21% 22%
[libx264 @ 0x5628b8e51f40] i8 v,h,dc,ddl,ddr,vr,hd,vl,hu: 28% 15% 36%  4%  3%  4%  3%  4%  4%
[libx264 @ 0x5628b8e51f40] i4 v,h,dc,ddl,ddr,vr,hd,vl,hu: 31% 18% 22%  5%  4%  4%  5%  7%  4%
[libx264 @ 0x5628b8e51f40] i8c dc,h,v,p: 63% 16% 18%  2%
[libx264 @ 0x5628b8e51f40] Weighted P-Frames: Y:2.6% UV:0.4%
[libx264 @ 0x5628b8e51f40] ref P L0: 46.7%  9.4% 29.7% 13.8%  0.4%
[libx264 @ 0x5628b8e51f40] ref B L0: 88.6%  9.2%  2.2%
[libx264 @ 0x5628b8e51f40] ref B L1: 97.4%  2.6%
[libx264 @ 0x5628b8e51f40] kb/s:4075.18
vagrant@ubuntu-bionic:~/ip_video_aa/library$
``` 

> **Note:** You can also convert the H.264 file to `mpeg` and compare the file size and video quality, by changing the output file extension, e.g. `ffmpeg -i AXISM1014_2022_07_24_183025.264 AXISM1014_2022_07_24_183025.mpeg`

```shell
vagrant@ubuntu-bionic:~/ip_video_aa/library$ ls -salt AXISM1014_2022_07_24_183025*
 88304 -rw-rw-r-- 1 vagrant vagrant  90422889 Jul 24 18:43 AXISM1014_2022_07_24_183025.mp4
 15420 -rw-rw-r-- 1 vagrant vagrant  15790080 Jul 24 18:39 AXISM1014_2022_07_24_183025.mpeg
188392 -rw-rw-r-- 1 vagrant vagrant 192906715 Jul 24 18:35 AXISM1014_2022_07_24_183025.264
````

The output file(s), e.g. `AXISM1014_2022_07_21_162637.mp4`, can be opened and viewed using VLC. You can use Secure Copy Protocol (SCP) to retrieve the files from the guest virtual machine to the host laptop. Exit the ssh session from the virtual machine and issue `vagrant ssh-config`.

> **Note:** SCP uses uppercase `P` to switch ports! To eliminate errors with host key checking on the ephemerial Vagrant machines, specify `-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no`.

```shell
scp -i .vagrant/machines/default/virtualbox/private_key -P 2222 vagrant@127.0.0.1:/home/vagrant/ip_video_aa/library/AXISM1014_2022_07_24_183025.mp4 ./AXISM1014_2022_07_24_183025.mp4
AXISM1014_2022_07_24_183025.mp4                                                       100%   86MB  44.3MB/s   00:01

scp -i .vagrant/machines/default/virtualbox/private_key -P 2222 vagrant@127.0.0.1:/home/vagrant/ip_video_aa/library/AXISM1014_2022_07_24_183025.mpeg ./AXISM1014_2022_07_24_183025.mpeg
AXISM1014_2022_07_24_183025.mpeg                                                      100%   15MB  27.3MB/s   00:00
````

Using VLC on the host machine, Open the downloaded files, you will see the video clips from the Axis camera.

## Glossary
To understand the logic flow of the solutions, an understanding of a number of terms is required.

***Network Abstraction Layer (NAL)***  Part of the H.264 standard, which enable the video stream to be transported over an IP network.

***NAL Units*** Individual IP packets in the video stream.

***Video Coding Layer (VCL)***  Each NAL unit can be classified as VCL or non-VCL. non-VCL NAL units contain any associated additional information such as parameter sets, essentially metadata. The VCL NAL units contain the data that represents the values of the samples in the video pictures.

***IDR (Instantaneous Decoder Refresh)*** An I-Frame that specifies no subsequent frame can reference a previous frame. Every every IDR frame is an I-frame.

***non-IDR frame*** Either a P-Frame or B-Frame.

***Group of Pictures (GOP)*** AKA Group of Video (GOV), configurable on the Axis camera, it specifies the frequency I-Frames are sent.

***RTP Marker bit*** The marker bit has a value of 1 indicating all the packets that make up an I-Frame or P-Frame have been received.

***Parameter Sets*** There are two types of parameter sets, SPS and PPS. In the Axis implementation, these parameter sets are sent "out-of-band" using the RTSP (TCP based) mechanism rather than the UDP based RTP transport. In this implementation, they are read from files at the beginning of the program execution. Refer to Session Description Protocol (SDP) values configured in the environment variables. 

***Sequence Parameter Set (SPS)*** SPS NAL unit contains parameters that apply to a series of consecutive coded video pictures. They have a nal_unit_type value of '7'.

***Picture Parameter Set (PPS)*** PPS NAL unit contains parameters that apply to the decoding of one or more individual pictures inside a coded video sequence. They have a nal_unit_type value of '8'.

> **Note:** The SPS and PPS do not change. The Axis implementation does **NOT** generate them in the always-on RTP stream. They are only send via the RTSP exchange, essentially sent *Out of Band* from the multicast stream. We use Wireshark to discern the bytes of these files and save them as binary files. We read the files at the beginning of the program execution. When a new file is opened in this solution, a new SPS and PPS are written as the first bytes of the output file. 

### Network Abstraction Layer Units
The AXIS camera uses the specified NAL Unit Types, the last 5 bits of the H.264 FU Identifier, the first byte after the RTP header.

```
0      Unspecified                                                    non-VCL
1      Coded slice of a non-IDR picture                               VCL        <--- P-Frame NAL Unit Type 1 >
2      Coded slice data partition A                                   VCL
3      Coded slice data partition B                                   VCL
4      Coded slice data partition C                                   VCL
5      Coded slice of an IDR picture                                  VCL         <--- I-Frame NAL Unit Type 5 >
6      Supplemental enhancement information (SEI)                     non-VCL
7      Sequence parameter set                                         non-VCL     <- SPS >
8      Picture parameter set                                          non-VCL     <- PPS >
9      Access unit delimiter                                          non-VCL
10     End of sequence                                                non-VCL
11     End of stream                                                  non-VCL
12     Filler data                                                    non-VCL
13     Sequence parameter set extension                               non-VCL
14     Prefix NAL unit                                                non-VCL
15     Subset sequence parameter set                                  non-VCL
16     Depth parameter set                                            non-VCL
17..18 Reserved                                                       non-VCL
19     Coded slice of an auxiliary coded picture without partitioning non-VCL
20     Coded slice extension                                          non-VCL
21     Coded slice extension for depth view components                non-VCL
22..23 Reserved                                                       non-VCL
24..31 Unspecified   
```

## Wireshark Decodes
The NETWORK Abstraction Layer Units shown above are decoded and explained using Wireshark, refer to [WIRESHARK](./documentation/WIRESHARK.md).

## Contributing
Details can be found in our [CONTRIBUTING](./CONTRIBUTING.md).

## License
Details can be found in our [LICENSE](./LICENSE).

## Support
Use the issue tracker to suggest feature requests, report bugs, and ask questions.

## Roadmap
The Session Description Protocol files are manually created by using Wireshark to examine an RTSP exchange between VLC and the camera to build the SPS and PPS files. This should be done programmatically.

Converting the raw H.264 files output from the `capture_video.py` program should be done in a shell script and then the H.264 file deleted. Optionally upload the MP4 file to a cloud (or private) object store database with the appropriate tags and meta data to facilitate searching and retrieval.

## Project status
At the point the repository is transitioned from private to public, I consider this to be minimum viable product. Adding roadmap items will be based on time and interest.

## Authors and acknowledgment
[@joelwking](https://gitlab.com/joelwking) developed the code base with lots of help from Stack Overflow and other available sources. See the [REFERENCE](documentation/REFERENCE.md) section for more details.
