# Background
A bit of my background regarding my past experience with IP Video Surveillance.
## Cisco
During my tenure at `cisco Systems, Inc.` I developed and published Cisco Validated Design (CVD) guides focused on QoS enabled IPSec encrypted voice and video. At that time, Cisco was marketing ***[medianet](https://www.cisco.com/c/dam/en_us/solutions/medianet/docs/ent_medianet_QA.pdf)***
an intelligent QoS enabled network optimized for rich media.  A component of *medianet* was a focus on the Video Surveillance and Access Control industry through their Physical Security Business Unit (PSBU). 
 
 As a member of the Cisco Enterprise Systems Engineering team, I was the author of the [Cisco IP Video Surveillance Design Guide](https://www.slideshare.net/joelwking/cisco-ip-video-surveillance-design-guide). At Networkers 2009 (CiscoLive) my session BRKVVT-2311 - **Network Design and Implementation for IP Video Surveillance** was the highest rated session of the 25 talks in the Voice/Video track. I also presented on this topic at the security industry trade show ISC West in 2010.

 Cisco announced the the end-of-sale and end-of-life dates for the Cisco Video Surveillance Manager 7. Cisco Meraki has entered the video surveillance market with an offering of cloud managed cameras which [store](https://documentation.meraki.com/Architectures_and_Best_Practices/Cisco_Meraki_Best_Practice_Design/Best_Practice_Design_-_MV_Cameras/Meraki_MV_Cameras_-_Introduction_and_Features) video on the camera itself (edge recording). Edge recording eliminates the need for a video management software to capture and archive the video feeds, however, if the camera is destroyed or damages, the recorded video may be as well. Edge recording is attractive in a small business deployment and typically not permitted in the more regulated gaming environments.

## NetApp
In 2011, NetApp acquired LSI's Engenio storage platform and marketed it as the E-Series family of storage systems.  The E-Series is a reliable block storage solution for Big Data applications which included video surveillance. In 2011, the 2TB drives became available (followed by the 3TB), the E-Series offerings could scale from entry level to approximately a petabyte (1,024 terabytes) depending on the model. I joined NetApp in July of 2011 as a member of a solutions engineering team focusing on IP video surveillance as a Big Data solution for the E-Series. The team developed and published the [Video Surveillance Solutions Using NetAPP E-Series Storage](https://www.slideshare.net/joelwking/tr-4233-videosurveillancesolutionsusingnetappeseriesstorage). During my time at NetApp, I had the opportunity to gain a considerable amount of hands-on experience with VMware's virtualization (ESXi) in addition to storage.

NetApp continues to market the [NetApp Video Surveillance Storage (VSS) solutions](https://www.netapp.com/data-storage/unstructured-data/video-surveillance-storage/) using the NetApp E-Series storage system.

## Axis Communications
Axis is the industry leader in network video, offering the first commercially available IP camera in 1996.  I completed the examination for the [Axis Certified Professional](https://www.axis.com/learning/academy/axis-certification-program) in June 2011. It is not an overly complex exam, you must know the basics of Video Surveillance and be famililiar with the Axis camera models and typical deployments. A very valuable reference for any professional is the book:

> Intelligent Network Video: Understanding Modern Video Surveillance Systems, Second Edition 2nd Edition by Fredrik Nilsson SBN-13: 978-1466555211

This book is really the definative reference for those interested in network video.

# State of the Industry
NetApp continues to focus on the high-end of the market through their relationship with Axis and Milestone, while Cisco has transitioned to the low-end with the Meraki cameras. 

Canon Inc. purchased Milestone Systems (Video Management software) in 2014 and Axis Communications in 2015. This merged the two market leaders in video management software and network cameras.