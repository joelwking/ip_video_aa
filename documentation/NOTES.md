# IP Video Archiver and Analytics
This project receives IP Multicast RTP/H.264 video streams from Axis Communication IP Video Surveillance cameras and writes H.264 files. It effectively is a lightweight Python video management software solution. 

The genesis of this project stemed from a need to validate the performance charastics and limits of storage systems used for video archive. 

A challenge in developing validated solutions at scale is how to accurately generate data to report performance of the network, compute and storage. There is camera simulator software, like the Axis network camera emulator v.0.1. This simulator runs on Windows 2008 R2 on physical machines. The simulator is configured to connect to a physical camera, accept an input video feed, and replicate that video feed for a given number of virtual cameras. 

While virtual camera simulators and other performance tools can be used, video from real cameras provides a higher degree of credibility in the results.  By using the multicast capability of the Axis cameras, the network can replicate a single video feed into any number of network interfaces. By configuring trunk port(s) between a Linix host and the L3 switch, any number of VLANs (virtual LANs) can be configured on a single physical interface. By invoking the Python code in this solution to listen for multicast packets on each VLAN, a single camera feed can be received from the network and written to the storage system.

A typical video surveillance deployment maintains a one to one (1:1) relationship between a camera and the receiver function of the video management software. This solution uses the network to allow for a one to many (1:n) relationship between a camera and the receiver.

# Background
A bit of my background regarding IP Video Surveillance.
## Cisco
During my tenure at `cisco Systems, Inc.` I developed and published Cisco Validated Design (CVD) guides focused on QoS enabled IPSec encrypted voice and video. At that time, Cisco was marketing ***[medianet](https://www.cisco.com/c/dam/en_us/solutions/medianet/docs/ent_medianet_QA.pdf)***

 *an intelligent network optimized for rich media. It is media-, endpoint- and network-aware to ensure an optimal quality of the total experience while automating many aspects of configuration and optimization.* 
 
 A component of *medianet* was a focus on the Video Surveillance and Access Control industry through their Physical Security Business Unit (PSBU). 
 
 As a member of the Cisco Enterprise Systems Engineering team, I was the author of the [Cisco IP Video Surveillance Design Guide](https://www.slideshare.net/joelwking/cisco-ip-video-surveillance-design-guide). At Networkers 2009 (CiscoLive) my session BRKVVT-2311 - **Network Design and Implementation for IP Video Surveillance** was the highest rated session of the 25 talks in the Voice/Video track. I also presented on this topic at the security industry trade show ISC West in 2010.

## NetApp
In 2011, NetApp accquired LSI's Engenio storage platform and marketed it as the E-Series family of storage systems.  The E-Series is a reliable block storage solution for Big Data applications which included video surveillance. In 2011, the 2TB drives became available (followed by the 3TB), the E-Series offerings could scale from entry level to approximately a petabyte (1,024 terabytes) depending on the model. I joined NetApp in July of 2011 as a member of a solutions engineering team focusing on IP video surveillance as a Big Data solution for the E-Series. The team developed and published the [Video Surveillance Solutions Using NetAPP E-Series Storage](https://www.slideshare.net/joelwking/tr-4233-videosurveillancesolutionsusingnetappeseriesstorage). During my time at NetApp, I had the opportunity to gain a considerable amount of hands-on experience with VMware's virtualization (ESXi) in addition to storage.

## State of the Industry
Cisco announced the the end-of-sale and end-of-life dates for the Cisco Video Surveillance Manager 7. Cisco Meraki has entered the video surveillance market with an offering of cloud managed cameras which [store](https://documentation.meraki.com/Architectures_and_Best_Practices/Cisco_Meraki_Best_Practice_Design/Best_Practice_Design_-_MV_Cameras/)Meraki_MV_Cameras_-_Introduction_and_Features) video on the camera itself (edge recording). Edge recording eliminates the need for a video management software to capture and archive the video feeds, however, if the camera is destroyed or damages, the recorded video may be as well. Edge recording is attractive in a small business deployment and typically not permitted in the more regulated gaming environments.

NetApp markets the [NetApp Video Surveillance Storage (VSS) solutions]https://www.netapp.com/data-storage/unstructured-data/video-surveillance-storage/) using the NetApp E-Series storage system.

NetApp continues to focus on the high-end of the market through their relationship with Axis and Milestone, while Cisco has transitioned to the low-end with the Meraki cameras. 

Canon Inc. purchased Milestone Systems (Video Management software) in 2014 and Axis Communications in 2015. 

## Analytics
The real value in video is the advanced analytics of the video streams which provide business value over and above the loss control and forsensic analysis of nefarious activity.  Cisco Meraki emphasis the use of out-of-the box analytics for people and vehicle detection along with a partner ecosystem to use the camera as a platform for retail analytics.


## Glossary
To understand the logic flow of the solutions, an understanding of a number of terms is required.

Network Abstraction Layer (NAL) - part of the H.264 standard, which enable the video stream to be transported over an IP network.
NAL Units - are individual IP packets in the video stream.
IDR (Instantaneous Decoder Refresh) - an I-Frame that specifies no subsequent frame can reference a previous frame. Every every IDR frame is an I-frame.
non-IDR frame - Either a P-Frame or B-Frame.
Group of Pictures (GOP) - AKA Group of Video (GOV), configurable on the Axis camera, it specifies the frequency I-Frames are sent.
RTP Marker bit - The marker bit has a value of 1 indicating all the packets that make up an I-Frame or P-Frame have been received.



## Topology

![Topology](documentation/Topology_Overview.png "Solution Topology")

## Multicast Primer
IP multicast is a means to conserve bandwidth and deliver packets to multiple receivers without adding any additional burden on the application sending the packets. Multimedia streaming applications, which include IP Video Surveillance, can deliver their content via multicast. This section shows how to configure the Axis camera software to generate RTP/H.264 packets over multicast and a brief overview of how multicast works.

IPV4 addresses (groups) in the range 224.0.0.0 and 239.255.255.255 are reserved for multicasting. 

Using the Axis web interface and setup option, the RTP port range and multicast settings are configured under System Options > Network > RTP. By selecting the `Always Multicast Video` multicast streaming is enabled without opening an RTSP session to the camera.  The multicast group (IP) address, port number, and Time-To-Live value are configured as well. **TALK ABOUT USING A  DIFFERENT GROUP PER CAMERA.

The program WILL send out IGMP Join Group when started and Leave Group when terminated.

Multicasting is designed for streaming multimedia applications and for conferencing systems in which one transmitting machines needs to distribute data to a large number of clients. IP V4 addresses (groups) in the range 224.0.0.0 and 239.255.255.255 are reserved for multicasting. These addresses do not correspond to individual machines, but to multicast groups. Messages sent to these addresses will be delivered to a potentially large number of machines that have registered their interest in receiving transmissions on these groups. They work like TV channels. A program tunes in to a multicast group to receive transmissions to it, and tunes out when it no longer wishes to receive the transmissions.

 https://metacpan.org/pod/IO::Socket::Multicast

... from the IPmc Design Guide
IP multicast is a means to conserve bandwidth and deliver packets to multiple receivers without adding any additional burden on the source or receivers of the packets. Applications that are implemented to deliver their data content by way of IP multicast include videoconferencing, IP/TV broadcasts of company meetings, distribution of files or software packages, real-time price quotes of securities trading, news and even video feeds from IP video surveillance cameras.. 

While the sender and receiver of the IPmc stream do not incur any additional burden of sending or receiving a IPmc stream, regardless of the number of receivers present in the network, the routers in the network do consume additional CPU resources.  The routers consume additional resources with both the control plane of IP multicast and the data replication function. 

The control plane is made up primarily of Internet Group Management Protocol (IGMP) and Protocol Independent Multicast (PIM). Routers listen to IGMP messages from hosts on their local networks and periodically send out queries to discover which groups are active or inactive

The program WILL send out IGMP Join Group when started and Leave Group when terminated.

## Process Flow


## Wireshark
In Wireshark, use Analyze 'Decode as' 

```
decode_as_entry: rtp.pt,96,RAD (RFC2198),H.264
decode_as_entry: udp.port,50000,(none),RTP
```


Once you determie the RTP Payload type (DynamicRTP-Type-96), EDIT->PREFERENCES-PROTOCOLS -> H264 and enter in dynamic payload the integer 96.

This allows you to visualize the RTP header and the H.264 FU identifier and FU header.

## Development Environment
These steps describe how to set up a development environment for testing code in an ephemerial environment.

Set up a [`bridged network adapter`](https://docs.oracle.com/en/virtualization/virtualbox/6.0/user/network_bridged.html) and associate it with the laptop Ethernet interface. On this laptop, Internet connectivity is via the Wireless LAN interface. Connect the Ethernet interface to the Axis camera Etheret interface. Configure the camera to use the default values for a fixed IP address.

Verify the camera is configured to `always multicast video` under **System Options** -> **Network** -> **RTP Settings**.

>Note: If you are multicasting video, you may not be able to Live View the video, as some camera models are very limited in performance of their CPU. You will see an error, ***If no image is displayed, there might be too many viewers***!

You can also specify a **Stream Profile** for multicast under **Video** setup and reference it in the **RTP** configuration.

```
  |=   Ubuntu VM  =|
 |==   Virtual Box  ==|
=== Intel x86 Laptop  ===     Ethernet <------ + ------>  Axis Camera 192.168.0.90/24 
```

>Note: To factory reset, power off the camera, press and hold the reset button on the back of the camera for 15-30 seconds as you apply power to the camera.

Edit the netplan configuration `/etc/netplan/00-installer-config.yaml`, assign an IP address to the adapter. By default Axis cameras will use 192.168.0.90/24 as their Ethernet if DHCP is not available from factory reset.

```yaml
# This is the network config written by 'subiquity'
network:
  ethernets:
    enp0s3:
      dhcp4: true
    enp0s8:
      dhcp4: true
    enp0s9:
            addresses:
             - 192.168.0.1/24
            # gateway4: 192.168.0.254   If you assign a gateway, you cannot reach the internet via the NAT interface

  version: 2
```

>Note: if you do not have a web browser on the Ubuntu instance, you can assign an IP address to the Laptop's Ethernet interface using the same IP address shown above, to reconfigure the camera to use DHCP.

Apply the configuration to the virtual machine and verify the interface is up and configured.

```shell
sudo netplan apply
```

```shell
$ ifconfig enp0s9
enp0s9: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.1  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::a00:27ff:fe09:72dd  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:09:72:dd  txqueuelen 1000  (Ethernet)
        RX packets 388080  bytes 540777032 (540.7 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1098  bytes 66886 (66.8 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

Verify you are getting multicast packets. Optionally filter on the MAC address of the camera.

```shell
$ sudo tcpdump -i enp0s9 ether host 00:40:8c:f9:ae:17
```
Wait about 20-30 seconds, and you should see TCPDUMP output.

```
administrator@flint:~/ip_video_aa/library$ sudo tcpdump -i enp0s9 ether  host 00:40:8c:f9:ae:17
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp0s9, link-type EN10MB (Ethernet), capture size 262144 bytes
22:09:19.303965 IP 192.168.0.90.50000 > 239.201.174.23.50000: UDP, length 1400
22:09:19.305040 IP 192.168.0.90.50000 > 239.201.174.23.50000: UDP, length 1400
22:09:19.305044 IP 192.168.0.90.50000 > 239.201.174.23.50000: UDP, length 1400
22:09:19.305045 IP 192.168.0.90.50000 > 239.201.174.23.50000: UDP, length 1400
22:09:19.305046 IP 192.168.0.90.50000 > 239.201.174.23.50000: UDP, length 1400
22:09:19.305047 IP 192.168.0.90.50000 > 239.201.174.23.50000: UDP, length 1400
```
>Note: if the camera does not have an IP address assigned and `always stream multicast` is enabled, the source IP will be 0.0.0.0, the program will not receive these packets, as they don't match the interface subnet specified in the code.

Now in the code snippit, the data portion of the packet is up to 1400 bytes, the IP and UDP headers are 28 bytes (20 and 8 bytes).

```python
data, address = mcastsock.recvfrom(BUFFER_SIZE)
src_ip, src_port = address
print(f'Received {len(data)}\tbytes from {src_ip} {src_port} Buffering')
```

## FFMPEG
The utility [`ffmpeg`](https://ffmpeg.org/) is installed in the development Docker container (see `Dockerfile`). This utility is a prereq for OpenCV. It is also installed in the VirtualBox Linux instance.

>Note: Issue the command with -loglevel debug

```shell
(ip_video_aa) root@cf032320c346:/workspaces/ip_video_aa/test/data# ffmpeg -i AXIS_2022_07_04_130752_head.264 AXIS_2022_07_04_130752_head.mpeg -loglevel debug
ffmpeg version 4.1.9-0+deb10u1 Copyright (c) 2000-2022 the FFmpeg developers
  built with gcc 8 (Debian 8.3.0-6)
  [snip]
Splitting the commandline.
Reading option '-i' ... matched as input url with argument 'AXIS_2022_07_04_130752_head.264'.
Reading option 'AXIS_2022_07_04_130752_head.mpeg' ... matched as output url.
Reading option '-loglevel' ... matched as option 'loglevel' (set logging level) with argument 'debug'.
Finished splitting the commandline.
Parsing a group of options: global .
Applying option loglevel (set logging level) with argument debug.
Successfully parsed a group of options.
Parsing a group of options: input url AXIS_2022_07_04_130752_head.264.
Successfully parsed a group of options.
Opening an input file: AXIS_2022_07_04_130752_head.264.
[NULL @ 0xaaaac5ab4a20] Opening 'AXIS_2022_07_04_130752_head.264' for reading
[file @ 0xaaaac5a92700] Setting default whitelist 'file,crypto'
[h264 @ 0xaaaac5ab4a20] Format h264 detected only with low score of 1, misdetection possible!
[h264 @ 0xaaaac5ab4a20] Before avformat_find_stream_info() pos: 0 bytes read:1014413 seeks:0 nb_streams:1
[h264 @ 0xaaaac5ab59d0] missing picture in access unit with size 1014413
[AVBSFContext @ 0xaaaac5a92040] nal_unit_type: 7(SPS), nal_ref_idc: 3
[AVBSFContext @ 0xaaaac5a92040] nal_unit_type: 8(PPS), nal_ref_idc: 3
[AVBSFContext @ 0xaaaac5a92040] nal_unit_type: 28(Unspecified 28), nal_ref_idc: 3
    Last message repeated 95 times
```

A NAL Unit 28 is a fragmented 

## Hex Viewer
Having a HEX Viewer is helpful. Sublime text [Hex Viewer (a hex viewer and editor](https://forum.sublimetext.com/t/hex-viewer-a-hex-viewer-and-editor/3132) is used.

## Hex to binary
Enter hex values into this page, and save it as a binary file http://tomeko.net/online_tools/hex_to_file.php?lang=en

The sample file from https://stackoverflow.com/questions/24884827/possible-locations-for-sequence-picture-parameter-sets-for-h-264-stream/24890903#24890903 was entered into Sublime, and all but hex characters were removed. The resulting file `complete_AU.264` is saved.

Using `complete_AU.264` paste the hex falues in the page, then save it as a binary file, under filename `myfile.264`. Then run this H264 file through `ffmpeg`.

```shell
root@daacf9c5ac45:/workspaces/ip_video_aa/documentation# ffmpeg -i myfile.264 myfile.mpeg
ffmpeg version 4.1.9-0+deb10u1 Copyright (c) 2000-2022 the FFmpeg developers
  built with gcc 8 (Debian 8.3.0-6)
  configuration: --prefix=/usr --extra-version=0+deb10u1 --toolchain=hardened --libdir=/usr/lib/aarch64-linux-gnu --incdir=/usr/include/aarch64-linux-gnu --arch=arm64 --enable-gpl --disable-stripping --enable-avresample --disable-filter=resample --enable-avisynth --enable-gnutls --enable-ladspa --enable-libaom --enable-libass --enable-libbluray --enable-libbs2b --enable-libcaca --enable-libcdio --enable-libcodec2 --enable-libflite --enable-libfontconfig --enable-libfreetype --enable-libfribidi --enable-libgme --enable-libgsm --enable-libjack --enable-libmp3lame --enable-libmysofa --enable-libopenjpeg --enable-libopenmpt --enable-libopus --enable-libpulse --enable-librsvg --enable-librubberband --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libssh --enable-libtheora --enable-libtwolame --enable-libvidstab --enable-libvorbis --enable-libvpx --enable-libwavpack --enable-libwebp --enable-libx265 --enable-libxml2 --enable-libxvid --enable-libzmq --enable-libzvbi --enable-lv2 --enable-omx --enable-openal --enable-opengl --enable-sdl2 --enable-libdc1394 --enable-libdrm --enable-libiec61883 --enable-chromaprint --enable-frei0r --enable-libx264 --enable-shared
  libavutil      56. 22.100 / 56. 22.100
  libavcodec     58. 35.100 / 58. 35.100
  libavformat    58. 20.100 / 58. 20.100
  libavdevice    58.  5.100 / 58.  5.100
  libavfilter     7. 40.101 /  7. 40.101
  libavresample   4.  0.  0 /  4.  0.  0
  libswscale      5.  3.100 /  5.  3.100
  libswresample   3.  3.100 /  3.  3.100
  libpostproc    55.  3.100 / 55.  3.100
Input #0, h264, from 'myfile.264':
  Duration: N/A, bitrate: N/A
    Stream #0:0: Video: h264 (High), yuv420p(progressive), 64x64, 25 tbr, 1200k tbn, 50 tbc
Stream mapping:
  Stream #0:0 -> #0:0 (h264 (native) -> mpeg1video (native))
Press [q] to stop, [?] for help
[mpeg1video @ 0xaaaae09ed940] too many threads/slices (5), reducing to 4
[mpeg @ 0xaaaae0995b20] VBV buffer size not set, using default size of 230KB
If you want the mpeg file to be compliant to some specification
Like DVD, VCD or others, make sure you set the correct buffer size
Output #0, mpeg, to 'myfile.mpeg':
  Metadata:
    encoder         : Lavf58.20.100
    Stream #0:0: Video: mpeg1video, yuv420p, 64x64, q=2-31, 200 kb/s, 25 fps, 90k tbn, 25 tbc
    Metadata:
      encoder         : Lavc58.35.100 mpeg1video
    Side data:
      cpb: bitrate max/min/avg: 0/0/200000 buffer size: 0 vbv_delay: -1
frame=    1 fps=0.0 q=2.2 Lsize=       2kB time=00:00:00.04 bitrate= 409.5kbits/s speed=5.99x    
video:2kB audio:0kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 11.546841%
root@daacf9c5ac45:/workspaces/ip_video_aa/documentation# 
```

Then open the file in VLC. 
>Note: because the file is only one frame, it will flash on the screen and end immediately, I placed it in a playlist with a rtsp:// stream that required entering a username and password, so the file would remain on the screen.

## RTCP packets
The camera will send out a RTCP packet prior to starting the RTP stream. Source and destination port of 500001 (one higher than the port number for the RTP stream that contains the Sender SSRC value. This value is the Synchronization Source identifier in each of the RTP packets that follow.

>Note: TODO, my code will not receive these packets because the port is set to 50000 in the current configuration.

## Streaming multicast RTP/H.264
The RTP port range and multicast settings are configured under System Options > Network > RTP.

Select Always Multicast Video and/or Always Multicast Audio to start multicast streaming without opening an RTSP session.

## Customizing IGMP
Configuring the Device to Forward Multicast Traffic in the Absence of Directly Connected IGMP Hosts. https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/ipmulti_igmp/configuration/xe-16/imc-igmp-xe-16-book/imc-customizing-igmp.html

Configure static group membership entries on an interface. With this method, the device does not accept the packets itself, but only forwards them. Hence, this method allows fast switching.

```pre
interface gigabitethernet 1
  ip igmp static-group 225.2.2.2
```


## Cameras

 - [AXIS M1004-W: Front: 192.168.5.90](http://192.168.5.90)
 - [AXIS M1014: Bonus: 192.168.5.91](http://192.168.5.91)
 - [AXIS M3004: Office: 192.168.5.92](http://192.168.5.92)

## Analytics using AWS service
Amazon Rekognition offers pre-trained and customizable computer vision (CV) capabilities to extract information and insights from your images and videos.

https://aws.amazon.com/rekognition/
https://docs.aws.amazon.com/code-samples/latest/catalog/python-rekognition-rekognition_video_detection.py.html

## Reference charts



## Session Description Protocol

In VLC, you can specify the username and password, as `rtsp://root:password@192.168.5.91/axis-media/media.amp` or if left off the URL, VLC will prompt you for the username/password.

You need [Digest Auth with RTSP](https://stackoverflow.com/questions/55379440/rtsp-video-streaming-with-authentication). 

From the above ** A Digest Enabled RTSP server should respond to a DESCRIBE request with 401 unauthorized See D.2.2, returning a nonce in the WWW-Authenticate header, e.g. **
```
Real Time Streaming Protocol
    Response: RTSP/1.0 401 Unauthorized\r\n
    CSeq: 3\r\n
    WWW-Authenticate: Digest realm="AXIS_00408CF9AE17", nonce="00024728Y997472419d935d0ad37e60ecd3d35a43f0882e", stale=FALSE\r\n
    WWW-Authenticate: Basic realm="AXIS_00408CF9AE17"\r\n
    Date: Thu, 16 Jun 2022 14:39:18 GMT\r\n
```
The client then uses the nonce to generate the authorization header that can be used for authentication, 
```
Real Time Streaming Protocol
    Request: DESCRIBE rtsp://192.168.5.91:554/axis-media/media.amp RTSP/1.0\r\n
    CSeq: 4\r\n
    Authorization: Digest username="root", realm="AXIS_00408CF9AE17", nonce="00024728Y997472419d935d0ad37e60ecd3d35a43f0882e", uri="rtsp://192.168.5.91:554/axis-media/media.amp", response="edc354e5ff6d42a826e1b162798f8078"\r\n
    User-Agent: LibVLC/3.0.17.3 (LIVE555 Streaming Media v2016.11.28)\r\n
    Accept: application/sdp\r\n
```
The response from the camera will be a RTSP packet with the Session Description Protocol information.

Session Description Protocol
    Session Description Protocol Version (v): 0
    Owner/Creator, Session Id (o): - 1655390359076847 1655390359076847 IN IP4 192.168.5.91
    Session Name (s): Media Presentation
    E-mail Address (e): NONE
    Bandwidth Information (b): AS:50000
    Time Description, active time (t): 0 0
    Session Attribute (a): control:*
        Session Attribute Fieldname: control
        Session Attribute Value: *
    Session Attribute (a): range:npt=0.000000-
        Session Attribute Fieldname: range
        Session Attribute Value: npt=0.000000-
    Media Description, name and address (m): video 0 RTP/AVP 96
        Media Type: video
        Media Port: 0
        Media Protocol: RTP/AVP
        Media Format: DynamicRTP-Type-96
    Connection Information (c): IN IP4 0.0.0.0
    Bandwidth Information (b): AS:50000
    Media Attribute (a): framerate:30.0
        Media Attribute Fieldname: framerate
        Media Attribute Value: 30.0
    Media Attribute (a): transform:1.000000,0.000000,0.000000;0.000000,0.959821,0.000000;0.000000,0.000000,1.000000
        Media Attribute Fieldname: transform
        Media Attribute Value: 1.000000,0.000000,0.000000;0.000000,0.959821,0.000000;0.000000,0.000000,1.000000
    Media Attribute (a): control:trackID=1
        Media Attribute Fieldname: control
        Media Attribute Value: trackID=1
    Media Attribute (a): rtpmap:96 H264/90000
        Media Attribute Fieldname: rtpmap
        Media Format: 96
        MIME Type: H264
        Sample Rate: 90000
    Media Attribute (a): fmtp:96 packetization-mode=1; profile-level-id=4D4029; sprop-parameter-sets=Z01AKZpmAoAy2AtQEBAQXpw=,aO48gA==
        Media Attribute Fieldname: fmtp
        Media Format: 96 [H264]
        Media format specific parameters: packetization-mode=1
        Media format specific parameters: profile-level-id=4D4029
        Media format specific parameters: sprop-parameter-sets=Z01AKZpmAoAy2AtQEBAQXpw=,aO48gA==


You can use the Python library `rtsp` to initiate and obtain the Session Description Protocol from the Axis cameras. Recall that  the SPS and PPS are sent `out of band` from the RTP/H264 stream. You need to obtain this information using RTSP, then you can apply the SPS and PPS to the beginning of each file you write on disk.


From Wireshark, looking at the RTSP Reply, we see the SPS in hex


SPS = b'\x67\x4d\x40\x29\x9a\x66\x02\x80\x2d\xd8\x0b\x50\x10\x10\x10\x5e\x9c'

SPS = b'\x67\x42\x00\x0a\xf8\x41\xa2'


Refer to [](https://doc-kurento.readthedocs.io/en/latest/knowledge/h264.html#sps-pps)

```
*Details about the exact contents of PPS and SPS packets can be found in the [H.264](https://www.itu.int/rec/T-REC-H.264/) Specification sections “Sequence parameter set data syntax” and “Picture parameter set RBSP syntax”.

Parameter sets can be sent in-band with the actual video, or sent out-of-band via some channel which might be more reliable than the transport of the video itself. This second option makes sense for transports where it is possible that some corruption or information loss might happen; losing a PPS could prevent decoding of a whole frame, and losing an SPS would be worse as it could render a whole chunk of the video impossible to decode, so it is important to transmit these parameter sets via the most reliable channel.*
```
Logic description
-----------------
```
    def handle_fu_hdr(self, data):
        """
        H.264 deploys three kinds of frames; I-frames, also called key frames, are complete frames stored without 
        reference to any other frame. P-frames and B-frames leverage redundancies from other frames.
        We don't expect to encounter B frames with the Axis camera implementation.

        While P-frames may be fragmented, we don't expect to encounter them. I-frames will typically be multiple MTU packets,
        with the last packet less than the MTU value and have the RTP Marker set as well as the H.264 End bit set.

        The first packet of a FU-A picture will have the start bit set in the H.264 header, but there is no start field in the
        RTP header. We must insert the h.264 start code b'\x00\x00\x00\x01' for both P and I-frames when the start bit is set.

        Until we encounter an I-frame (with the start bit set), we must discard all P-frames, and any I-frames.  We discard frames
        by simply returning an empty string. 

        https://stackoverflow.com/questions/7665217/how-to-process-raw-udp-packets-so-that-they-can-be-decoded-by-a-decoder-filter-i
        If fragment_type == 28 then video data following it represents the video frame fragment. Next check is start_bit set, if it is, 
        then that fragment is the first one in a sequence. You use it to reconstruct IDR's NAL byte by taking the first 3 bits from 
        first payload byte (3 NAL UNIT BITS) and combine them with last 5 bits from second payload byte (5 NAL UNIT BITS) 
        so you would get a byte like this [3 NAL UNIT BITS | 5 NAL UNIT BITS]. Then write that NAL byte first into a clear buffer 
        with VIDEO FRAGMENT DATA from that fragment.

        If start_bit and end_bit are 0 then just write the VIDEO FRAGMENT DATA (skipping first two payload bytes that identify the 
        fragment) to the buffer.

        If start_bit is 0 and end_bit is 1, that means that it is the last fragment, and you just write its VIDEO FRAGMENT DATA 
        (skipping the first two bytes that identify the fragment) to the buffer, and now you have your video frame reconstructed!
        """
```
P-frame analysis and debugging
------------------------------
```
Frame 3672: 1442 bytes on wire (11536 bits), 1442 bytes captured (11536 bits) on interface \Device\NPF_{4D7411CA-745E-4082-8DD4-5BB648695792}, id 0
Ethernet II, Src: AxisComm_f9:ae:17 (00:40:8c:f9:ae:17), Dst: IPv4mcast_49:ae:17 (01:00:5e:49:ae:17)
    Destination: IPv4mcast_49:ae:17 (01:00:5e:49:ae:17)
    Source: AxisComm_f9:ae:17 (00:40:8c:f9:ae:17)
    Type: IPv4 (0x0800)
Internet Protocol Version 4, Src: 192.168.0.90, Dst: 239.201.174.23
User Datagram Protocol, Src Port: 50000, Dst Port: 50000
    Source Port: 50000
    Destination Port: 50000
    Length: 1408
    Checksum: 0x21ae [unverified]
    [Checksum Status: Unverified]
    [Stream index: 0]
    [Timestamps]
    UDP payload (1400 bytes)
Real-Time Transport Protocol
H.264
    FU identifier
        0... .... = F bit: No bit errors or other syntax violations
        .11. .... = Nal_ref_idc (NRI): 3
        ...1 1100 = Type: Fragmentation unit A (FU-A) (28)
    FU Header
        1... .... = Start bit: the first packet of FU-A picture
        .0.. .... = End bit: Not the last packet of FU-A picture
        ..0. .... = Forbidden bit: 0
        ...0 0001 = Nal_unit_type: Coded slice of a non-IDR picture (1)
    H264 NAL Unit Payload
        1... .... = first_mb_in_slice: 0
        .001 10.. = slice_type: P (P slice) (5)
        .... ..1. = pic_parameter_set_id: 0
        [Not decoded yet]

Note that the first three bits of the FU ID and last 5 of the FU Header, give us 0110 0001 or 0x61 rather than 0x65 so you need to write the method to build that byte for P-Frames rather than hard code a 0x65

IDR means instantaneous decoding refresh (IDR) picture

https://www.engeniustech.com/technical-papers/H.264-video-compression.pdf

Looking at the debug, you can indeed have multiple packets in P-Frames

It looks like you should have an I-frame, and the GOV number of P-Frames, then another I-Frame
```


```
    def strip_rtp_hdr(self, data, debug=False):
        """
          Strip RTP (Real-Time Transport Protocol) header
          https://www.rfc-editor.org/rfc/rfc3550

          hdr = bytes.fromhex('80e0259b7e6189f0a8698434')

          Struct documentation https://docs.python.org/3/library/struct.html

          10.. .... = Version: RFC 1889 Version (2)
          ..0. .... = Padding: False
          ...0 .... = Extension: False
          .... 0000 = Contributing source identifiers count: 0

          0... .... = Marker: False   96 decimal no marker, decimal 224, marker
          Payload type: DynamicRTP-Type-96

          Sequence: 2 bytes
          Timestamp: 4 bytes
          Synchronization Source identifier: 4 bytes

          Marker is the last packet of Access Unit.
          An H.264 file contains NAL (Network Abstraction Layer) units separated by start codes
          RFC 6184
          Reassemble NAL unites & write to disk with start codes 0x'00 00 00 01'

        """
```
```
    def get_bits(self, byte):
        """
          Input is an unsigned integer representation of a byte from a header,
          returned is an 8 character string representing the bits, '1's and '0's

          Note:  int('1111100', 2)  returns 124
                 int('01111100', 2) returns 124
                 byte1 = 0b01111100  assigns integer 124 to byte1
                 hex(0b01111100) returns '0x7c'
                 0x7c returns 124
        """
```

```
class H264_Header(object):
    """
        https://yumichan.net/video-processing/video-compression/breif-description-of-nal_ref_idc-value-in-h-246-nalu/

                Fragmentation Unit (FU)
        
        FU identifier (one byte)
          0... .... = F bit: No bit errors or other syntax violatios
          .11. .... = Nal_ref_idc (NRI): 3
          ...1 1100 = Type: Fragmentation unit A (FU-A) (28)
                      0x7c  I frame
                      0x61  P frame
        FU Header (one byte)
          1... .... = Start bit: the first packet of FU-A picture
          .0.. .... = End bit: Not the last packet of FU-A picture
          ..0. .... = Forbidden bit: 0
          ...0 0001 = Nal_unit_type: Code slice of a non-IDR picture (1)
                      0x81
                1 Coded slice of a non-IDR picture                               
                7 Sequence parameter set            Session Description Protocol (RTSP communication)          
                8 Picture parameter set                 |

        H264 NAL Unit Payload follows
        
        # Fragmentation Unit H.264
        # IFRAME        '01111100'      # 0x7c
        IFRAME           = '11100'         # Fragmentation unit A (FU-A) (28) (where 28 is the integer value)
        PFRAME           = '01100001'      # 0x61
        FU_HDR_START_BIT = '1'
        FU_HDR_END_BIT   = '01000001'      # 0x41
```
```
   # TODO Remove these variables
    # https://www.cardinalpeak.com/blog/the-h-264-sequence-parameter-set
    # These values come from Wireshark reviewing the RTSP DESCRIBE output for 192.168.5.92
    # Picture Parameter Set (PPS)  NAL_Unit_type 0x68
    # PPS = b'\x68\xee\x3c\x80'
    # Sequence Parameter Set (SPS) NAL_Unit_type 0x67
    # SPS = b'\x67\x4d\x40\x29\x9a\x66\x02\x80\x2d\xd8\x0b\x50\x10\x10\x10\x5e\x9c'
```

Boilerplate
-----------

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/joelwking/ip_video_aa.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/joelwking/ip_video_aa/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
