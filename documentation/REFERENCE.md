# References

These references are used as research and learning resources for developing the solution software.

## Python

- [Python bitwise operators Struct documentation](https://docs.python.org/3/library/struct.html)
- [What is Bit Masking?](https://stackoverflow.com/questions/10493411/what-is-bit-masking#10493604)
- [Python IP Multicast](https://pymotw.com/2/socket/multicast.html)

## Storage

- [E-Series Storage for Video Surveillance](https://www.netapp.com/pdf.html?item=/media/19898-wp-7240.pdf)

## RTP/RTSP/RTCP

- [How to write RTP H264 stream as a file](https://stackoverflow.com/questions/9581778/how-to-write-rtp-h264-stream-as-a-file)
- [RTP Payload Format for H.264 Video](https://datatracker.ietf.org/doc/html/rfc6184)
- [Can't Explain RTSP Authentication (Basic and Digest)](https://stackoverflow.com/questions/25332235/cant-explain-rtsp-authentication-basic-and-digest)
- [RTSP Python](https://stackoverflow.com/questions/64103535/python-receive-rtsp-stream-from-ip-camera)
- [RTSP](https://pypi.org/project/rtsp/)
- [Python script to show live video using RTSP](https://aruljohn.com/blog/python-live-video-rtsp/)
- [RTSP H264 does not get sps/pps from DESCRIBE by parsing sprop-parameter-sets ](https://github.com/meetecho/janus-gateway/issues/849)
- [rtp decoding issue on p frames](https://stackoverflow.com/questions/23274697/rtp-decoding-issue-on-p-frames)

## FFMPEG

- [FFMPEG](https://www.videoproc.com/resource/ffmpeg-commands.htm)
- [ffmpeg: A complete, cross-platform solution to record, convert and stream audio and video](https://ffmpeg.org/)
- [H.264 Video Encoding Guide ](https://trac.ffmpeg.org/wiki/Encode/H.264)

## H.264

- [Network Abstraction Layer](https://en.wikipedia.org/wiki/Network_Abstraction_Layer)
- [Overview of H.264 Video Compressions](https://www.engeniustech.com/technical-papers/H.264-video-compression.pdf)
- [Non IDR Picture NAL Units - 0x21 and 0x61 meaning](https://stackoverflow.com/questions/22551325/non-idr-picture-nal-units-0x21-and-0x61-meaning)
- [SPS-PPS](https://doc-kurento.readthedocs.io/en/latest/knowledge/h264.html#sps-pps)
- [H.264 spec](https://www.itu.int/rec/dologin_pub.asp?lang=e&id=T-REC-H.264-202108-I!!PDF-E&type=items)
- [H.264 extradata (partially) explained - for dummies](http://aviadr1.blogspot.com/2010/05/h264-extradata-partially-explained-for.html)
 - [H.264 over RTP - Identify SPS and PPS Frames](https://stackoverflow.com/questions/9618369/h-264-over-rtp-identify-sps-and-pps-frames)
- [H.264 Sequence Parameter Set](https://www.cardinalpeak.com/blog/the-h-264-sequence-parameter-set)
- [Kurento H.264 video codec](https://doc-kurento.readthedocs.io/en/latest/knowledge/h264.html)
- [ntroduction to H.264: (1) NAL Unit](https://yumichan.net/video-processing/video-compression/introduction-to-h264-nal-unit/)
- [Possible Locations for Sequence/Picture Parameter Set(s) for H.264 Stream](https://stackoverflow.com/questions/24884827/possible-locations-for-sequence-picture-parameter-sets-for-h-264-stream/24890903#24890903)
- [usage of start code for H264 video](https://stackoverflow.com/questions/28421375/usage-of-start-code-for-h264-video#29103276)
- [Beginners Guide to encoding H264](https://streaminglearningcenter.com/codecs/beginners-guide-to-encoding-h264.html)

## Axis Cameras

- [RTSP commands for Axis Cameras](https://learncctv.com/rtsp-commands-for-axis-cameras/)
- [AXIS M1065-LW Network Camera](https://www.axis.com/files/manuals/um_m1065lw_1632649_en_1709.pdf)
- [Axis GitHub](https://github.com/AxisCommunications/)
- [Axis Technology Platform - emphasis on utilizing the new H.264 standard](https://www.axis.com/files/manuals/MigrationGuide_v1_04.pdf)
- [pyasixcam](https://github.com/danielorf/pyaxiscam)
- [Axis VAPIX(r) Library](https://www.axis.com/vapix-library/subjects/t10175981/section/t10035974/display)
- [Multicast on Axis Cameras](https://learncctv.com/multicast-on-axis-cameras/)

## Networking

- [Processing raw UDP packets](https://stackoverflow.com/questions/7665217/how-to-process-raw-udp-packets-so-that-they-can-be-decoded-by-a-decoder-filter-i)

## Wireshark

- [Decoding RTP payload as H264 using wireshark](https://stackoverflow.com/questions/26164442/decoding-rtp-payload-as-h264-using-wireshark)

## OpenCV | Pillow

- [OpenCV: Open Source Computer Vision Library](https://github.com/opencv/)
- [Writing an mp4 video using python opencv](https://stackoverflow.com/questions/30509573/writing-an-mp4-video-using-python-opencv)
- [Intel: What is OpenCV?](https://www.intel.com/content/www/us/en/developer/articles/technical/what-is-opencv.html)
- [Python Imaging Library](https://thecleverprogrammer.com/2021/08/21/python-imaging-library-pil-tutorial/)
- [Pillow, fork of PIL](https://python-pillow.org/)
- [Intro to Pillow](https://www.blog.pythonlibrary.org/2016/10/07/an-intro-to-the-python-imaging-library-pillow/)
- [Writing an mp4 video using python opencv](https://stackoverflow.com/questions/30509573/writing-an-mp4-video-using-python-opencv)

## Video viewing software

 - [VLC: Video LAN](https://www.videolan.org/)
 - [XnView](https://www.xnview.com/en/)
 - [XnConvert](https://www.xnview.com/en/xnconvert/)
 
 