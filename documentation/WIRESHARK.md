# Wireshark Decodes

### Wireshark
Wireshark is a very useful tool for analyzing and learning the various NAL units sent by the Axis Camera. Use the 'Analyze' -> 'Decode As' option, and configure these two entries.

```
# "Decode As" entries file for Wireshark 3.6.6.
#
# This file is regenerated each time "Decode As" preferences
# are saved within Wireshark. Making manual changes should be safe,
# however.
decode_as_entry: rtp.pt,96,RAD (RFC2198),H.264
decode_as_entry: udp.port,50000,(none),RTP
```
> Note: the example assumes the Axis camera is configured for port 50000.

### Wireshark decode of the RTP Header

This example shows the RTP header of a P-Frame. The program verifies the Payload type is `DynamicRTP-Type-96` and warns if there are any lost RTP packets by verifying the RTP sequence number of the current packet with the previous packet. The program also reports whe the RTP sequence number wraps. In this example, the Marker bit is set (True), indicating this is the last packet in a fragment. In the Axis implementation, P-Frames typically are not fragmented, resulting in the Marker bit set for every P-Frame.

```
Real-Time Transport Protocol
    10.. .... = Version: RFC 1889 Version (2)
    ..0. .... = Padding: False
    ...0 .... = Extension: False
    .... 0000 = Contributing source identifiers count: 0
    1... .... = Marker: True
    Payload type: DynamicRTP-Type-96 (96)
    Sequence number: 35579
    Timestamp: 1877575021
    Synchronization Source identifier: 0xa166d773 (2707871603)
```

### Wireshark decode of the identified NAL Units.

This example is the first packet of an I-Frame (Coded slice of an IDR picture)

The bytes following the RTP header are 0x7c 0x85
```
H.264
    FU identifier
        0... .... = F bit: No bit errors or other syntax violations
        .11. .... = Nal_ref_idc (NRI): 3
        ...1 1100 = Type: Fragmentation unit A (FU-A) (28)
    FU Header
        1... .... = Start bit: the first packet of FU-A picture
        .0.. .... = End bit: Not the last packet of FU-A picture
        ..0. .... = Forbidden bit: 0
        ...0 0101 = Nal_unit_type: Coded slice of an IDR picture (5)
    H264 NAL Unit Payload
        1... .... = first_mb_in_slice: 0
        .000 1000 = slice_type: I (I slice) (7)
        1... .... = pic_parameter_set_id: 0
        [Not decoded yet]
            [Expert Info (Warning/Undecoded): [Not decoded yet]]
```
The packet following the above is the second fragment, bytes following the RTP header 0x7c 0x05 - the start bit is not set.
```
H.264
    FU identifier
        0... .... = F bit: No bit errors or other syntax violations
        .11. .... = Nal_ref_idc (NRI): 3
        ...1 1100 = Type: Fragmentation unit A (FU-A) (28)
    FU Header
        0... .... = Start bit: Not the first packet of FU-A picture
        .0.. .... = End bit: Not the last packet of FU-A picture
        ..0. .... = Forbidden bit: 0
        ...0 0101 = Nal_unit_type: Coded slice of an IDR picture (5)

```

The last fragment will have the RTP Marker bit set (not shown below), and will have the End bit set in the H.264 header. The hex representation of these two bytes is 0x7c 0x45.
```
H.264
    FU identifier
        0... .... = F bit: No bit errors or other syntax violations
        .11. .... = Nal_ref_idc (NRI): 3
        ...1 1100 = Type: Fragmentation unit A (FU-A) (28)
    FU Header
        0... .... = Start bit: Not the first packet of FU-A picture
        .1.. .... = End bit: the last packet of FU-A picture
        ..0. .... = Forbidden bit: 0
        ...0 0101 = Nal_unit_type: Coded slice of an IDR picture (5)

```

Following the I-Frame, is a P-Frame, it is not fragmented, the RTP header has the Marker bit set. The two bytes followig the RTP header are 0x61 0x9a.

```
H.264
    NAL unit header or first byte of the payload
        0... .... = F bit: No bit errors or other syntax violations
        .11. .... = Nal_ref_idc (NRI): 3
        ...0 0001 = Type: NAL unit - Coded slice of a non-IDR picture (1)
    H264 NAL Unit Payload
        1... .... = first_mb_in_slice: 0
        .001 10.. = slice_type: P (P slice) (5)
        .... ..1. = pic_parameter_set_id: 0
        [Not decoded yet]

```
If the Group of Video (Group of Pictures) is set to 32, there will be 31 P-Frames following the fragmented packets of the I-Frame, and then a new I-Frame.

These packets are called, Video Coding Layer (VCL), because they define the video image. We also need meta data to decode the video, and this is represented by the Parameter Sets.

### Parameter Sets

The Axis cameras do not transmit the Sessio Description Protocol NAL Units with the multicast stream. This is metadata used to decode the video stream. To build the H.264 file(s), each new file must first write the Sequence Parameter Set and the Picture Parameter Set NAL Units. 

The Axis camera will transmit these values in an RTSP exchange between VLC (VideoLAN Client) and the Axis camera. Wireshark is used to capture the exchange and copy the values of the SPS and PPS and save them in files(s) which are then read by the receiving program, and used to populate each H.264 file.

Using VLC, open a network connection to each camera, (substitute the IP address of the camera with the sample value shown).

```url
rtsp://192.168.5.90/axis-media/media.amp
```

> Note: If the camera requires a username and password, it can either be sent in the RTSP command `rtsp://username:password@192.168.5.90/axis-media/media.amp` or entered when prompted by VLC.

Using Wireshark, identify the **DESCRIBE** issued by VLC, for example:

```
Request: DESCRIBE rtsp://192.168.5.90:554/axis-media/media.amp RTSP/1.0\r\n
    Method: DESCRIBE
    URL: rtsp://192.168.5.90:554/axis-media/media.amp
```

Locate the **Reply** packet:

```
Real Time Streaming Protocol
    Response: RTSP/1.0 200 OK\r\n
        Status: 200
    CSeq: 7\r\n
    Content-type: application/sdp
    Content-Base: rtsp://192.168.5.90:554/axis-media/media.amp/\r\n
    Date: Sun, 24 Jul 2022 13:32:05 GMT\r\n
    Content-length: 471
    \r\n
    Session Description Protocol
```

Expand the **Session Description Protocol**

The SPS begins with 0x67, and PPS begins with 0x68, these two hex strings must be written to files accessable by the receiving program and specified by using the environment variables.

```
Media format specific parameters: sprop-parameter-sets=Z01AKZpmBQF/y4C1AQEBBenA,aO48gA==
    NAL unit 1 string: Z01AKZpmBQF/y4C1AQEBBenA
    NAL unit: 674d40299a6605017fcb80b501010105e9c0
    NAL unit 2 string: aO48gA==
    NAL unit: 68ee3c80
```

One method of creating the SPS and PPS files, is to use Python, for each the SPS and PPS, create a file with the camera identifier as the file basename, and use `.sps` and `.pps` as the file extension. Copy the hex strings from wireshark, escape the hex characters and write the byte object to the appropriate file, the close the file. 

```python
>>> f = open('/tmp/axis12345.sps', 'w+b')
>>> sps = b'\x68\xee\x3c\x80'
>>> f.write(sps)
4
>>> f.close()
>>> quit()
```
> Note: the return value from the `f.write` command is the number of bytes written.

The file basename is passed to the receiving program by setting the envionment variable:

```shell
export AA_SD_FILENAME=/tmp/axis12345
```

