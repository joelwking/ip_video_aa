#!/usr/bin/env python3
#      Copyright (c) 2022  Joel W. King
#      All rights reserved
#
#      author: Joel W. King GitHub/GitLab @joelwking
#
#      description:
#
#        Captures RTP/H.264 video streamed via IP Multicast
#
#      usage:
#        python ./library/caputure_video.py
#
#      references:
#        https://unix.stackexchange.com/questions/420545/multicast-frames-in-linux-bridge-dropped
#        https://interestingtraffic.nl/2017/11/21/an-oddly-specific-post-about-group_fwd_mask/
#
import sys
import signal
import os
import time
#
#  User imports
#
import ip_video_aa as ip_video_aa
from logger import logger
#
#  Constants and program input
#
COPYRIGHT = "\nCopyright (c) 2022 by Joel W. King\n"
#
#  Pass all run-time values from environment variables
#
DPORT = int(os.getenv('AA_DPORT', 50000))
IF_IP = os.getenv('AA_IF_IP', '192.168.0.1')
MULTICAST_GROUP = os.getenv('AA_MULTICAST_GROUP', '239.201.174.23')
BUFFER_SIZE = os.getenv('AA_BUFFER_SIZE', 16384)                           # at 4K, you may see RTP packet loss, use 8K or greater
#
#  Logging options
#
opts = dict(level=int(os.environ.get('AA_LOGLEVEL', 20)),
            file_name=(os.environ.get('AA_LOG_FILE', None)),              # If not specified, no log file will be written
            logger_name=os.getenv('AA_CAMERA', 'AXIS'))                   # Camera name in each log entry

log = logger.Logger(**opts).setup()
#
#  Pass the ip_video_aa Class parameters as a keywords.
#
parameters = dict(max_io_bytes=os.getenv('AA_MAX_IO_BYTES', 4),            # Write buffer to disk every n MBYTES, default 4MB
                  sd_filename=os.getenv('AA_SD_FILENAME', 'default_sdp'),  # Session Description Protocol (SDP) filename (basename)
                  camera=os.getenv('AA_CAMERA', 'AXIS'),                   # Prefix for output files 
                  directory=os.getenv('AA_DIRECTORY', './'),               # Directory to store SDP files and write output
                  max_file_open=os.getenv('AA_MAX_FILE_OPEN', 5),          # Number of minutes between opening new files
                  debug=False,                                             # Debug switch
                  logger=log)                                              # logger object

if opts['level'] == 10:                                                    # TODO revisit this code  DEBUG = 10
    parameters['debug'] = True
#
#  Join the Multicast Group
#
mcastsock = ip_video_aa.join_multicast(MULTICAST_GROUP, DPORT, IF_IP)

log.info(f'Listening {MULTICAST_GROUP} {DPORT} on interface: {IF_IP}')
#
#  Enable interrupt handler
#
def sigint_handler(signum, frame):
    log.info(f'Interrupt SIGNAL:{signum}')
    mcastsock.close()
    sys.exit()

signal.signal(signal.SIGINT, sigint_handler)
#
#  Instanciate a buffer, to handle managing files and writing data
#
buffer = ip_video_aa.Buffer(**parameters)

def main():

    while True:
        data, address = mcastsock.recvfrom(BUFFER_SIZE)
        src_ip, src_port = address
        # print(f'Received {len(data)}\tbytes from {src_ip} {src_port} Buffering...')
        if src_port != DPORT:             # Only process packets where source and dest port are equal values
            log.debug(f'Received {len(data)}\tbytes from {src_ip} {src_port}')
            continue

        try:
            buffer.video(data)
        except ip_video_aa.AppError as e:
            log.error(f'AppError buffering video: {e}')
            return
        except AssertionError as e:
            log.error(f'AssertionError raised validating packets: {e}')
            return
                                                                       
if __name__ == '__main__':
    main()
