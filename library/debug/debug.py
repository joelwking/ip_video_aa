#
#   >>> from debug import debug
#       debugger = debug.Debug()
#       debugger.get_bits(124)
#   >>> ('0111 1100', '0x7c', 124)
#
class Debug(object):
    
    def __init__(self):
        return

    def get_bits(self, byte):
        """
        Debugging method to convert an integer value into bits and hex

        Args:
            byte (int): unsigned char (a struct decode of '!B'), byte order network (= big-endian)

        Raises:
            TypeError: Input was not integer of one byte

        Returns:
            tuple: return the bit string separated by a space, the hex representation and input integer
        """

        if not isinstance(byte, int) or byte > 255:
            raise TypeError('get_bits sent invalid value')

        bits = f'{bin(byte)[2:]:0>8}'    # '10000000' or '01010001' perhaps
        return (f'{bits[:4]} {bits[4:]}', hex(byte), byte )
