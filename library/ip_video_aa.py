#
#   ip_video_aa.py
#
import socket
import struct
import time
from xmlrpc.client import APPLICATION_ERROR
#
def join_multicast(mcast_addr,port,interface_ip):
    """
    Returns a live multicast socket

    Args:
        mcast_addr (str): dotted string format of the multicast group, e.g. 239.201.174.23
        port (int): integer of the UDP port you want to receive: e.g. 50000
        interface_ip (str): dotted string format of the interface to listen on: e.g. 192.168.0.1

    Reference: 
        http://stackoverflow.com/questions/3859090/choosing-multicast-network-interface-in-python
        https://stackoverflow.com/questions/603852/how-do-you-udp-multicast-in-python
    """
    #
    #              Create a UDP socket
    #
    mcastsock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    #   
    #              Allow other sockets to bind this port too
    #
    mcastsock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    #
    #              Explicitly join the multicast group on the interface specified
    #
    mcastsock.setsockopt(socket.SOL_IP,socket.IP_ADD_MEMBERSHIP,
                socket.inet_aton(mcast_addr)+socket.inet_aton(interface_ip))
    #
    #              Bind the socket (note: using tuple) to start getting data into your socket
    #              Note: to bind to all addresses use an empty string '' for mcast_addr, see reference above
    #                       
    mcastsock.bind((mcast_addr, port))

    return mcastsock

class AppError(Exception):
    """
      Raise Custom Error: AppError This exception is used to identify unexpected conditions in the program logic
    """
    pass

class RTP_Header(object):
    """
    Class to decode the RTP header
    """
    def __init__(self, logger):
        """
        RTP header format: https://www.rfc-editor.org/rfc/rfc3550
        
            0                   1                   2                   3
            0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
            |V=2|P|X|  CC   |M|     PT      |       sequence number         |
            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
            |                           timestamp                           |
            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
            |           synchronization source (SSRC) identifier            |
            +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
            |            contributing source (CSRC) identifiers             |
            |                             ....                              |
            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        """
        self.log = logger
        # BYTE 1
        self.byte1 = None
        self.version = 0                             # 10.. ....  RFC 1889 Version 2
        self.version_mask = 0b11000000               # Mask to decode version
        self.rfc1889_version2 = 0b10000000           # 10.. ....  RFC 1889 Version 2
        self.padding = 0                             # ..0. ....
        self.extension = 0                           # ...0 ....
        self.contributing_source_identifiers = 0     # .... 0000
        # BYTE 2
        self.byte2 = None
        self.marker = 0                              # 0... ....  False (0)
        self.marker_mask = 0b10000000                # 1... ....  True (128)
        self.payload_type = 0                        # .110 0000  DynamicRTP-Type-96 (96)
        self.payload_type_mask = 0b01111111          # Mask to ignore the first bit from byte 2
        self.dynamic_rtp_type_96 =  0b01100000       # DynamicRTP-Type-96 (96) 
        # BYTES 3-4
        self.sequence_number = 0                     # .... ....             sequence number incremets for each RTP packet
        # BYTES 5-8
        self.timestamp = 0                           # .... .... .... ....   timestamp is the same for all fragments of a Fragmentation unit
        # BYTES 9-12
        self.ssi = 0                                 # .... .... .... ....   Synchronization Source identifier
                                                     #                       globally unique, to identify amoung multiple RTP sessions
        # Counters                                             
        self.previous_sequence_number = 0            # store the previous RTP sequence number to identify packet loss

    def decode(self, header):
        """
        Decode the RTP header. All integer fields are carried in network byte order, we use the '!' format option to unpack the fields.
        After unpacking the header, we do bitwise ANDs with the appropriate mask to select the individual fields in bytes 1 and 2.
        We use assertions to validate the values in the header. 
        
        The Marker bit is decoded, it represents the last packet in a fragment, however, we defer to the Start and Stop bits in the H.264
        header to re-assemble the packets.

        Most importantly, we check for packet loss by comparing the current RTP sequence number with the previous sequence number and 
        warn about packet loss.

        Args:
            header (str): The 12 byte RTP header which follows the UDP header
        """
        self.byte1, self.byte2, self.sequence_number, self.timestamp, self.ssi = struct.unpack('!BBHII', header)
        # (  128,        96,         9627,                 2120321520,     2825487412)

        # BYTE 1
        self.version = self.byte1 & self.version_mask
        assert self.version == self.rfc1889_version2, f"Invalid RTP Version: {self.byte1}"

        # BYTE 2
        self.marker = self.byte2 & self.marker_mask                # Bitwise AND if bit 1 == 1, returns 128, otherwise, 0
                                                                   # test using "if rtp.marker:""
        self.payload_type = self.byte2 & self.payload_type_mask    # Check the RTP payload type
        assert self.payload_type == self.dynamic_rtp_type_96, f"Invalid RTP Payload Type: {self.byte2}"

        # Check for packet loss
        if self.previous_sequence_number > 0:
            if (self.previous_sequence_number + 1) != self.sequence_number:
                if self.sequence_number == 0:
                    self.log.info(f'RTP sequence number wrapped: {self.previous_sequence_number}::{self.sequence_number}')
                else:
                    self.log.warn(f'RTP packet loss: {self.previous_sequence_number}::{self.sequence_number}')

        self.previous_sequence_number = self.sequence_number

        return None

class H264_Header(object):
    """
    Class to decode and handle the H.264 header
 
    """
    def __init__(self):
        """
        RTP Payload Format for H.264 Video https://datatracker.ietf.org/doc/html/rfc6184 

        First two bytes of a FU-A payload (follows RTP header)

            |  FU indicator |   FU header   |
            +---------------+---------------+
            |0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|
            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
            |F|NRI|  Type   |S|E|R|  Type   |
            +---------------+---------------+
        """
        # BYTE 1   First byte of the payload or FU Identifier
        self.byte1 = None
        self.fbit = 0                                # 0... .... = F bit: No bit errors or other syntax violations
        self.fbit_mask = 0b10000000                  # Mask to decode F bit
        self.nal_ref_idc = 0                         # .11. .... = Nal_ref_idc (NRI): 3
        self.nal_ref_idc_mask = 0b01100000           # Mask to decode Nal_ref_idc
        self.FIRST_THREE = 0b11100000                # first three bits of byte 1
        #
        self.fu_id_type = 0                          # ...1 1100 = Type: Fragmentation unit A (FU-A)
        self.fu_id_type_mask = 0b00011111            # Mask to decode FU indicator Type
        self.IFRAME = 0b00011100                     # 0x7c  I frame (28) Fragmentation unit A (FU-A) (28)
        self.PFRAME = 0b00000001                     # 0x61  P frame (1)  Coded slice of a non-IDR picture (1)
        
        # BYTE 2   Second byte of the payload, or FU Header 
        self.byte2 = None
        self.start_bit = 0
        self.start_bit_mask = 0b10000000             # start bit for I-frame, first_mb_in_slice for P frame
        self.end_bit = 0
        self.end_bit_mask = 0b01000000               # end bit for I-frame
                                                     
                                                     # value of '5' is Coded Slice of an IDR picture  or Coded slice of a non-IDR picture (1)
        self.fu_header_type = 0                      # Wireshark calls this NAL Unit Type, but RFC 6184 does not.
        self.fu_header_type_mask = 0b00011111        # last 5 bits of second byte

        # Reconstructed NAL byte
        self.reconstructed_NAL = 0

    def decode(self, header):
        """
        Decode the N.264 header
        """
        
        self.byte1, self.byte2 = struct.unpack('!BB', header)          # unpack the header
        
        # BYTE 1
        self.fbit = self.byte1 & self.fbit_mask                        # expect return value of 0
        assert self.fbit == 0, "h.264 bit error or syntax violations encountered"
        
        self.nal_ref_idc = (self.byte1 & self.nal_ref_idc_mask) >> 5   # expect return value of 0x96, then shift right 5 bits
        assert self.nal_ref_idc == 3, f"h.264 nal_ref_idc value {self.nal_ref_idc} out of program scope"

        self.fu_id_type = self.byte1 & self.fu_id_type_mask
        assert self.fu_id_type in (self.IFRAME, self.PFRAME), f"h.264 fu_id_type ({self.fu_id_type})out of scope"

        # BYTE 2
        self.start_bit = self.byte2 & self.start_bit_mask               # expect return 0 or 128, zero is false, 128 is true
        self.end_bit = self.byte2 & self.end_bit_mask                   # expect return 0 or 64, zero is false, 64 is true

        # Reconstruct the NAL header, using the first three bits of byte 1 and the last 5 bits of byte 2
        self.reconstructed_NAL = struct.pack('!B', (self.byte1 & self.FIRST_THREE) + (self.byte2 & self.fu_header_type_mask))
        
        return None

class Buffer(object):
    """
      Class to buffer input data, process the RTP packets into raw H.264 data, and write output
    """
    def __init__(self, camera='AXIS', directory='./', max_io_bytes=4, sd_filename='default_sdp', max_file_open=5, debug=False, logger=None):
    
        self.camera = camera                                     # Prefix for filenames
        self.directory = directory                               # Directory for outputfiles, logs, etc.
        self.ofile = None                                        # Generated from directory, camera and timestamps
        self.buffer = b''                                        # Output buffer
        self.max_io_bytes = int(max_io_bytes) * ( 1024 * 1024 )  # in MBytes
        self.max_file_open = int(max_file_open) * 60             # calculate seconds from minutes
        self.debug = debug                                       # debug option for optionally generating more verbose output
        self.log = logger
        self.filepath = dict(filepath=None, mode='w+b')          # parameters for opening files, which are written as binary
        #
        #  Instanciate the class to decode the RTP header
        #
        self.rtp = RTP_Header(logger)
        #
        #  Instanciate the class to decode the H264 header
        #
        self.h264 = H264_Header()
        #
        # Used to fast forward past fragments and P frames to locate the first iFrame when starting new file
        #
        self.first_iframe = False
        #
        #  Session Description Protocol, AXIS cameras do not send SDP periodically, rather as part of the RTSP exchange
        #
        self.h264_start_code = struct.pack('!I', 1)    # returns b'\x00\x00\x00\x01'
        
        self.pps = self.h264_start_code + self.read_session_description(filename=f'{self.directory}{sd_filename}.pps')
        self.sps = self.h264_start_code + self.read_session_description(filename=f'{self.directory}{sd_filename}.sps')
        #
        #  Counters (to accumulate and display statistics)
        #
        self.bytes = 0
        self.packets = 0
        self.files = 0
        self.start_time = self.file_start_time = time.time()

    def video(self, data):
        """
        Add data to buffer, closing and opening files as required.

        Args:
            data (str):  real-time transport protocol (RTP) packet from the socket
        """
        self.accumulate_stats()
        self.check_file_state()
        #
        # First strip the RTP header, then handle the Functional Unit (FU) header
        #
        data = self.handle_fu_hdr(self.strip_rtp_hdr(data))
        #
        # Is the output buffer full?
        #
        if len(self.buffer) + len(data) > self.max_io_bytes:
            # write output buffer
            if self.ofile:
                try:
                    self.accumulate_stats(_bytes=len(self.buffer), stat='bytes')
                    self.ofile.write(self.buffer)
                except (IOError, OSError, TypeError) as e:
                    raise AppError(f'IO Error: {e}')
            else:
                raise AppError(f'Logic error: data in buffer and no file object available')
        else:
            self.buffer += data    # simply append data to existing buffer
            return
        #
        # Add data to new buffer
        #
        self.buffer = data

        return

    def strip_rtp_hdr(self, data):
        """
        Analyze and strips the RTP (Real-Time Transport Protocol) header : https://www.rfc-editor.org/rfc/rfc3550

        Args:
            data (str): UDP packet payload, the RTP packet 

        Returns:
            str: RTP packet payload (minus the RTP header)
        """

        header = data[:12]                       # RTP header is typically 12 bytes, but TODO, check Contributing source identifers count
        payload = data[12:]
        
        self.rtp.decode(header)

        if self.rtp.marker and self.debug:
            self.log.debug(f'RTP Marker: PAYLOAD:{self.rtp.payload_type} SEQUENCE:{self.rtp.sequence_number} TIMESTAMP:{self.rtp.timestamp}')

        return payload

    def handle_fu_hdr(self, data):
        """
        Handle the Fragmentation Unit Header

        Args:
            data (str): RTP packet with the RTP header stripped off

        Returns:
            str: either an empty byte string (effectively dropping the packet) or the H.264 data minus the 2-byte header.
        """
        self.h264.decode(data[:2])     # The Fragmentation Unit (aka header) is 2 bytes

        if self.first_iframe:          # Have we started the file buffer with an I-Frame with the start bit is set?
            pass
        else:
            if self.h264.fu_id_type == self.h264.IFRAME and self.h264.start_bit:
                self.first_iframe = True
                self.log.info(f'I-Frame: Fragmentation unit A (FU-A) {self.h264.fu_id_type}, located the first I-frame!')
            else:
                # Ignore P-Frames and fragments of I-Frames, return an empty byte string
                self.log.info(f'Discarding FU type: {self.h264.fu_id_type}, we have not located the first I-frame')
                return b''
        #
        #  I-frames, (or key frames), complete frames without reference to any other frame, they usually are fragmented across packets
        #
        if self.h264.fu_id_type == self.h264.IFRAME:
            if self.h264.start_bit:
                # self.log.debug('IFRAME FU-A START IDR Slice')
                return self.h264_start_code + self.h264.reconstructed_NAL + data[2:]

            if not self.h264.start_bit and self.h264.end_bit:
               # Last fragment
               # self.log.debug('IFRAME FU-A END IDR Slice')                 # TODO don't need this logic except for debugging
               pass
            
            # Either the last fragment or middle a fragment, return all but the first two bytes
            return data[2:]

        #
        #  P-frames (and B-frames) leverage redundancies from other frames, they could be fragmented across packets
        #
        if self.h264.fu_id_type == self.h264.PFRAME:
            if self.first_iframe:
                if self.h264.start_bit:
                    self.log.debug(f'Got P-frame, start bit set, returning h.264 start code, reconstructed NAL, data of length {len(data)}')
                    return self.h264_start_code + self.h264.reconstructed_NAL + data[:2]
                else:
                    self.log.debug(f"Encountered fragmented P-frame at RTP sequence number {self.rtp.sequence_number}")
                    return data[:2]    # Fragmented P-frame
            else:
                self.log.info('Got P-frame, but we have not located the first I-frame, discarding')
                return b''             # Got a P-frame, but have not located first I-frame
        #
        #  B-frame perhaps? We don't expect to encounter B frames with the Axis camera implementation.
        #
        raise AppError(f'Logic error in handle_fu_header: FU ID Type: {self.h264.fu_id_type}')

    def read_session_description(self, filename='default_sdp.pps'):
        """
        Read the Session Description Protocol files, SPS and PPS

        https://www.cardinalpeak.com/blog/the-h-264-sequence-parameter-set
        These values come from Wireshark reviewing the RTSP DESCRIBE output for 192.168.5.92
        
        Picture Parameter Set (PPS)  NAL_Unit_type 0x68
        Sequence Parameter Set (SPS) NAL_Unit_type 0x67
        
        Args:
            filename (str, optional): Path and file name of the SPS and PPS files. Defaults to './default_sdp.pps'.

        Raises:
            AppError: The NAL Unit type must be either 0x67 or 0x68

        Returns:
            str: bit string of the file contents or None
        """
        self.log.info(f'Reading SDP file: {filename}')
        try:
            f = open(filename, 'rb')
        except (IOError, FileNotFoundError) as e:
            self.log.error(f'Error reading Session Description Protocols files: {e}')
        # TODO THIS LOGIC NEEDS WORK
        session_description = f.read()
        f.close()
        
        fu_id_type = struct.unpack('!B', session_description[:1])[0]
        assert hex(fu_id_type) in ('0x67', '0x68'), f'Session Description {hex(fu_id_type)} not valid.'
        
        return session_description
#
#   The following methods are used for managing output files
#
    def open_new_file(self):
        """
          Close the current file (if any) and open a new file.
          We use None in the file object variable to indicate a file isn't open.
        """
        try:  # If the file isn't open, we get an AttributeError, as you can't close NoneType
            self.ofile = self.ofile.close()   # Returns None
        except AttributeError as e:
            pass

        self.filepath['filepath'] = self.get_output_filename(prefix=f'{self.directory}/{self.camera}')
        self.ofile = self.get_fileobj(**self.filepath)
        self.log.info(f"Opening file: {self.filepath['filepath']}")

        if self.ofile:
            self.file_start_time = time.time()
        else:
            raise AppError(f'Error opening output file:{self.filepath}')

        #
        # Write the SPS ad PPS to the new file, as each new output file needs these values to decode the video
        # Negate the first_iframe variable so the logic will 'Fast Forward' to the first packet of FU-A I-frame.
        #
        self.ofile.write(self.sps)
        self.ofile.write(self.pps)
        self.first_iframe = False
        self.accumulate_stats(stat='files')
        self.display_stats()           # Display stats everytime we open a new file.

    def get_fileobj(self, filepath=None, mode='r'):
        """
        Get a file object, return the file object or False if errors occured

        Args:
            filepath (str, optional): file path of the file. Defaults to None.
            mode (str, optional): file permissions. Defaults to 'r'.
        """
        try:
            fileobject = open(filepath, mode)
        except (IOError, OSError, TypeError) as e:
            self.log.error(f'Error opening file {e}')
            return False

        return fileobject

    def get_output_filename(self, prefix='./AXIS'):
        """
        Returns an output file name for storing video.
        The filenames are based off an optional camera designation and time stamp. 
        Most video management software packages create a new file every 1-5 minutes.

        Args:
            prefix (str, optional): filename prefix. Defaults to './AXIS'.
        """

        year,month,day,hour,minute,sec,wday,yday,isdst =  time.localtime(time.time())

        return f'{prefix}_{year}_{month:02d}_{day:02d}_{hour:02d}{minute:02d}{sec:02d}.264'
    
    def check_file_state(self):
        """
        Determine if it is time to close the current file and open a new file
        """
        if not self.ofile and len(self.buffer) == 0:
            # buffer empty and no file open, assumption is program start
            self.open_new_file()
            return
        
        if self.ofile:
            # we have an open file, do we need to start a new one?
            if (time.time() - self.file_start_time) > self.max_file_open:
                # the file has been open long enough
                self.open_new_file()
        else:
            self.open_new_file()

#
#   The following methods accumulate and display statistics
#
    def accumulate_stats(self, _bytes=0, stat='packets'):
        """
        Update statistic counters
        
        Args:
            _bytes (int, optional): Number of bytes written to the file. Defaults to 0.
            stat (str, optional): Either 'packets', 'bytes', or 'files'. Defaults to 'packets'.
        """
        if stat == 'packets':
            self.packets += 1
            return
        if stat == 'bytes':
            self.bytes += _bytes
            return
        if stat == 'files':
            self.files += 1
            return
        
        raise AppError(f"accumulate_stats must be either be 'packets', 'bytes', or 'files', got '{stat}'")

    def display_stats(self):
        """
        Display accumulated statistics, formatted with commas

        Returns:
            tuple: current counter values for bytes, packets and files
        """

        self.log.info(f"Bytes: {self.bytes:,} Packets: {self.packets:,}  Files: {self.files:,}")
        return (self.bytes, self.packets, self.files)